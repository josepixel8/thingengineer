<?php
require_once('E:\xampp\htdocs\ThingEngineer\vendor\thingengineer\mysqli-database-class\MysqliDb.php');

//use MysqliDb\MysqliDb;

$db = new MysqliDb('localhost', 'root', '', 'employeedb', 3307, 'utf8', 'my_');

// Function to check if a given ID already exists in the database
function isDuplicateID($id)
{
    global $db;

    $db->where('id', $id);
    $result = $db->getOne('employee');

    return $db->count > 0;
}

// Check if form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Create operation
    if (isset($_POST['submit'])) {
        $id = $_POST['id'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $middle_name = $_POST['middle_name'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        if (!isDuplicateID($id)) {
            $data = array(
                'id' => $id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'middle_name' => $middle_name,
                'birthday' => $birthday,
                'address' => $address
            );

            if ($db->insert('employee', $data)) {
                echo "Employee created successfully.";
            } else {
                echo "Error: " . $db->getLastError();
            }
        } else {
            echo "Error: Employee with ID $id already exists.";
        }
    }

    // Update operation
    if (isset($_POST['update'])) {
        $id = $_POST['id'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $middle_name = $_POST['middle_name'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        // Update Query
        $data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'middle_name' => $middle_name,
            'birthday' => $birthday,
            'address' => $address
        );

        $db->where('id', $id);
        if ($db->update('employee', $data)) {
            echo "Employee updated successfully.";
        } else {
            echo "Error updating employee: " . $db->getLastError();
        }
    }
}

// Delete operation
if (isset($_GET['delete'])) {
    $id = $_GET['delete'];

    // Delete Query
    $db->where('id', $id);
    if ($db->delete('employee')) {
        echo "Employee deleted successfully.";
    } else {
        echo "Error deleting employee: " . $db->getLastError();
    }
}

// Read operation - Get All
$employees = $db->get('employee');
if ($db->count > 0) {
    $employees = $db->get('employee');
} else {
    $employees = [];
}

// Get Employee by ID
function retrieveEmployeeDetails($id)
{
    global $db;

    if (trim($id) === '' || empty($id) || $id == NULL) {
        $employees = $db->get('employee');
        foreach ($employees as $employee) {
            print_r($employee);
        }

        // Select Query
        if ($db->count > 0) {
            foreach ($employees as $employee) {
                print_r($employee);
            }
        }

        // Select Query
        if ($db->count > 0) {
            echo '<table>';
            foreach ($employees as $employee) {
                echo '
                <tr>
                    <td>ID:</td>
                    <td>' . $employee["id"] . '</td>
                </tr>
                <tr>
                    <td>First Name:</td>
                    <td>' . $employee["first_name"] . '</td>
                </tr>
                <tr>
                    <td>Last Name:</td>
                    <td>' . $employee["last_name"] . '</td>
                </tr>
                <tr>
                    <td>Middle Name:</td>
                    <td>' . $employee["middle_name"] . '</td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td>' . $employee["birthday"] . '</td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td>' . $employee["address"] . '</td>
                </tr>';
            }
            echo '</table>';
        }
    }

    if (!is_null($id)) {
        $db->where('id', $id);
        $employee = $db->getOne('employee');

        if ($db->count > 0) {
            echo '
            <table>
                <tr>
                    <td>ID:</td>
                    <td>' . $employee["id"] . '</td>
                </tr>
                <tr>
                    <td>First Name:</td>
                    <td>' . $employee["first_name"] . '</td>
                </tr>
                <tr>
                    <td>Last Name:</td>
                    <td>' . $employee["last_name"] . '</td>
                </tr>
                <tr>
                    <td>Middle Name:</td>
                    <td>' . $employee["middle_name"] . '</td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td>' . $employee["birthday"] . '</td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td>' . $employee["address"] . '</td>
                </tr>
            </table>';
        } else {
            echo "No employee found with the given ID.";
        }
    }
}

if (!isset($_GET['view']) || empty($_GET['view'])) {
    $id = null;
    retrieveEmployeeDetails($id);
} elseif (!empty($_GET['view'])) {
    $id = $_GET['view'];
    retrieveEmployeeDetails($id);
}

$db->disconnect();
?>

<!DOCTYPE html>
<html>

<head>
    <title>Employee Management System</title>
    <style>
        table {
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>

<body>
    <h2>Employee Management System</h2>

    <!-- Create or Update Employee -->
    <h3>Add or Update Employee</h3>
    <form method="post">
        <label for="id">ID:</label>
        <input type="text" name="id" required>
        <br>
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" required>
        <br>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" required>
        <br>
        <label for="middle_name">Middle Name:</label>
        <input type="text" name="middle_name">
        <br>
        <label for="birthday">Birthday:</label>
        <input type="date" name="birthday" required>
        <br>
        <label for="address">Address:</label>
        <input type="text" name="address" required>
        <br>
        <input type="submit" name="submit" value="Create">
        <input type="submit" name="update" value="Update">
    </form>

    <!-- List All Employees -->
    <h3>All Employees</h3>
    <?php if (count($employees) === 0) : ?>
        <p>No employees found.</p>
    <?php else : ?>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Middle Name</th>
                    <th>Birthday</th>
                    <th>Address</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($employees as $employee) : ?>
                    <tr>
                        <td><?php echo $employee['id']; ?></td>
                        <td><?php echo $employee['first_name']; ?></td>
                        <td><?php echo $employee['last_name']; ?></td>
                        <td><?php echo $employee['middle_name']; ?></td>
                        <td><?php echo $employee['birthday']; ?></td>
                        <td><?php echo $employee['address']; ?></td>
                        <td>
                            <a href="?delete=<?php echo $employee['id']; ?>" onclick="return confirmDelete();">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>

    <!-- Get Employee by ID -->
    <h3>Employee by ID</h3>
    <form method="get">
        <label for="id">View Employee ID:</label>
        <input type="text" name="view">
        <button onclick="retrieveEmployeeDetails()">Get</button>
    </form>
    <script>
        function confirmDelete() {
            return confirm("Are you sure you want to delete this employee?");
        }
    </script>
</body>

</html>
